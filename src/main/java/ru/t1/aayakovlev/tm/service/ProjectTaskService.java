package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface ProjectTaskService {

    void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException;

    void removeProjectById(@Nullable final String userId, @Nullable final String projectId) throws AbstractException;

    void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException;

}
