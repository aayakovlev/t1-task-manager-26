package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;

public interface ServiceLocator {

    @NotNull
    AuthService getAuthService();

    @NotNull
    CommandService getCommandService();

    @NotNull
    LoggerService getLoggerService();

    @NotNull
    ProjectService getProjectService();

    @NotNull
    ProjectTaskService getProjectTaskService();

    @NotNull
    PropertyService getPropertyService();

    @NotNull
    TaskService getTaskService();

    @NotNull
    UserService getUserService();

}
