package ru.t1.aayakovlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.User;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Show user profile.";

    @NotNull
    public static final String NAME = "user-show-profile";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW USER PROFILE]");
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

}
